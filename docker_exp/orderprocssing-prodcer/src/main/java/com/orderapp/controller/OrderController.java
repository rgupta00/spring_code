package com.orderapp.controller;

import java.util.UUID;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.orderapp.config.MessageConfig;
import com.orderapp.dto.Order;
import com.orderapp.dto.OrderStatus;

@RestController
public class OrderController {

	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	@PostMapping("order/{restName}")
	public String bookOrder(@RequestBody Order order, @PathVariable(name = "restName") String restName) {
		order.setOrderId(UUID.randomUUID().toString());
		//call rest call to rest service
		//payment gatway
		OrderStatus orderStatus=new OrderStatus(order, "PROCESSING", "order is placed");
		
		rabbitTemplate.convertAndSend(MessageConfig.DEMO_EXCHANGE, MessageConfig.DEMO_ROUTING_KEY, 
				orderStatus);
		return "success";
	}
}












