package com.empapp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {

	@GetMapping(path = "employee")
	public Employee getEmployee() {
		return new Employee(121, "raj", 4000);
	}
}