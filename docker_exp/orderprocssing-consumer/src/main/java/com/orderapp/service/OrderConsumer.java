package com.orderapp.service;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import com.orderapp.config.MessageConfig;
import com.orderapp.dto.OrderStatus;

@Service
public class OrderConsumer {

	@RabbitListener(queues = MessageConfig.QUEUE)
	public void processOrder(OrderStatus orderStatus) {
		//in real life u have to process it ...
		System.out.println(orderStatus);
	}
}
