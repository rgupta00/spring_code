package com.orderapp.config;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class MessageConfig {

	public static final String DEMO_ROUTING_KEY = "demo_routingKey";
	public static final String DEMO_EXCHANGE = "demo_exchange";
	public static final String QUEUE = "demo_queue";
	//define the Queue
	@Bean
	public Queue queue() {
		return new Queue(QUEUE);
	}
	
	//define exchange
	@Bean
	public TopicExchange exchnage() {
		return new TopicExchange(DEMO_EXCHANGE);
	}
	//bind que with exchange
	@Bean
	public Binding binding(Queue queue, TopicExchange exchange) {
		return  BindingBuilder.bind(queue).to(exchange).with(DEMO_ROUTING_KEY);
	}
	
	//message converter: it is going to covert java object ot json and json to java
	@Bean
	public MessageConverter connver() {
		return new Jackson2JsonMessageConverter();
	}
	
	//rabbit mq template: 
	@Bean
	public AmqpTemplate amqpTemplate(ConnectionFactory connectionFactory) {
		RabbitTemplate rabbitTemplate=new RabbitTemplate(connectionFactory);
		rabbitTemplate.setMessageConverter(connver());
		return rabbitTemplate;
	}
}
