package com.empapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class EmployeeController {

	@Autowired
	private RestTemplate restTemplate;
	
	@GetMapping(path = "employeeclient")
	public Employee getEmployee() {
		System.out.println();
		Employee employee=restTemplate.getForObject("http://producer:8080/employee",
				Employee.class);
		
		System.out.println(employee);
		return employee;
	}
}