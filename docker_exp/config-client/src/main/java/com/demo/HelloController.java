package com.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
@RefreshScope
@RestController
public class HelloController {

	@Value("${spring.datasource.url: default value}")
	private String dbUrl;
	
	@GetMapping(path = "hello")
	public String hello() {
		return dbUrl;
		
	}
}
