package com.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

@SpringBootApplication
public class DemoActuatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoActuatorApplication.class, args);
	}

	@Bean
	@Scope("prototype")
	public Emp emp() {
		return new Emp();
	}
}
