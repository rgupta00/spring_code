package com.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoCollection {

	@GetMapping(path="hello")
	public String sayHello() {
		return "hello to spring actuator";
	}
}
