package com.demo;

import org.springframework.web.bind.annotation.GetMapping;

public class Hello {

	@GetMapping(path="hello")
	public String sayHello() {
		return "hello to spring web";
	}
}
