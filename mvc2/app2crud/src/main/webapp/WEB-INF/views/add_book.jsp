<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Addbook form</title>
</head>
<body>
<form:form action="addbook" method="post" modelAttribute="book">
	<form:hidden path="id" value="0"/>
	<table>
		<tr>
			<td>Enter title</td>
			<td><form:input path="title"/></td>
		</tr>
		<tr>
			<td>Enter author</td>
			<td><form:input path="author"/></td>
		</tr>
		<tr>
			<td>Enter price</td>
			<td><form:input path="price"/></td>
		</tr>
		<tr>
			<td><input type="submit"></td>
		</tr>
	</table>
</form:form>
</body>
</html>