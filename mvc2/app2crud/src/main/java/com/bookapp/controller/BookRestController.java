package com.bookapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookapp.model.dao.Book;
import com.bookapp.model.service.BookService;

@RestController //   @Controller+ @ResponseBody(conervt java ->json with the help of parser)
@RequestMapping(path = "api")
public class BookRestController {

	private BookService bookService;

	@Autowired
	public BookRestController(BookService bookService) {
		this.bookService = bookService;
	}
	//------getting all books------
	@GetMapping(path = "book")
	public List<Book> getAllBooks(){
		return bookService.getAllBooks();
	}
	
	//------get an book by id------
	@GetMapping(path = "book/{id}")
	public Book getAnBook(@PathVariable(name = "id") int id){
		return bookService.getBookById(id);
	}
	
	//------add a book------
	//@RequestBody :used to convert json to java object
	@PostMapping(path = "book")
	public Book  addAnBook(@RequestBody Book book){
		return bookService.addBook(book);
	}
	
	
	//------delete a book------
	@DeleteMapping(path = "book/{id}")
	public Book deleteAnBook(@PathVariable(name = "id") int id){
		return bookService.deleteBook(id);
	}
	
	//------update the book------
	
	@PutMapping(path = "book/{id}")
	public Book updateAnBook(@PathVariable(name = "id") int id, @RequestBody Book book){
		return bookService.updateBook(id, book);
	}
}




