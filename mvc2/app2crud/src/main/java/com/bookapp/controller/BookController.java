package com.bookapp.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.bookapp.model.dao.Book;
import com.bookapp.model.service.BookService;

@Controller
public class BookController {

	private BookService bookService;

	@Autowired
	public BookController(BookService bookService) {
		this.bookService = bookService;
	}

	@GetMapping("/")
	public String home() {
		return "redirect:allbooks";
	}
	
	// -------get all books------------------
	@GetMapping(path = "allbooks")
	public ModelAndView getallBooks(ModelAndView mv) {
		mv.setViewName("allbooks");
		mv.addObject("books", bookService.getAllBooks());
		return mv;
	}

	// ------------delete a book ----deletebook
	@GetMapping(path = "deletebook")
	public String deleteBook(HttpServletRequest req) {
		int id = Integer.parseInt(req.getParameter("id"));
		bookService.deleteBook(id);
		return "redirect:allbooks";
	}

	// -------update a book----------------

	@GetMapping(path = "updatebook")
	public String getUpdateBook(HttpServletRequest req, Model model) {
		int id = Integer.parseInt(req.getParameter("id"));
		Book book = bookService.getBookById(id);
		// i want to pre populate the form
		model.addAttribute("book", book);
		return "update_book";
	}

	// ----------adding a new book-------------------
	@GetMapping(path = "addbook")
	public String getAddBook(Model model) {
		model.addAttribute("book", new Book());// this object goes into req scope
		return "add_book";
	}

	// ------- THIS METHOD IS UESD TWO TIME SAVE / UPDATE
	@PostMapping(path = "addbook")
	public String postAddBook(@ModelAttribute(name = "book") Book book) {
		System.out.println("------------------");
		System.out.println(book);
		System.out.println("------------------");
		if (book.getId() == 0) {
			bookService.addBook(book);
		} else {
			bookService.updateBook(book.getId(), book);
		}
		return "redirect:allbooks";
	}
}
