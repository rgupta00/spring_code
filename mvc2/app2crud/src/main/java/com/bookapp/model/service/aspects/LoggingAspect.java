package com.bookapp.model.service.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.bookapp.model.service.BookServiceImpl;

@Service
@Aspect
public class LoggingAspect {
	private Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

	@Around("@annotation(MyLogger)")
	public Object doLogging(ProceedingJoinPoint joinPoint) throws Throwable {
		long start = System.currentTimeMillis();
		Object value=	joinPoint.proceed();
			
		long end = System.currentTimeMillis();

		logger.info("method" + joinPoint.getTarget()+ ": take "+(end - start) + " ms to execute");
		return value;
	}
}
