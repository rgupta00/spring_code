package com.bookapp.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bookapp.model.dao.Book;
import com.bookapp.model.dao.BookDao;
import com.bookapp.model.service.aspects.MyLogger;

//SL =BL +CCC 
@Service(value = "bookService")
@Transactional
public class BookServiceImpl implements BookService {

	private BookDao dao;

	@Autowired
	public BookServiceImpl(BookDao dao) {
		this.dao = dao;
	}

	@MyLogger
	@Override
	public List<Book> getAllBooks() {
		List<Book> allBooks = dao.getAllBooks();
		return allBooks;
	}

	@Override
	public Book addBook(Book book) {
		return dao.addBook(book);
	}

	@Override
	public Book deleteBook(int id) {
		return dao.deleteBook(id);
	}

	@Override
	public Book updateBook(int id, Book book) {
		return dao.updateBook(id, book);
	}

	@Override
	public Book getBookById(int id) {
		return dao.getBookById(id);
	}

}
