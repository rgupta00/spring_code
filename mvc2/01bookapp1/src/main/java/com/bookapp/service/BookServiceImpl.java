package com.bookapp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bookapp.dao.Book;
import com.bookapp.dao.BookDao;
import com.bookapp.service.aspects.MyLogger;
import com.bookapp.service.exceptions.BookNotFoundException;


@Service
@Transactional
public class BookServiceImpl implements BookService {

	private BookDao bookDao;

	@Autowired
	public BookServiceImpl(BookDao bookDao) {
		this.bookDao = bookDao;
	}

	@Override
	public List<Book> getAllBooks() {
		return bookDao.findAll();
	}

	//aop
	@MyLogger
	@Override
	public Book addBook(Book book) {
		bookDao.save(book);
		return book;
	}

	@Override
	public Book updateBook(int id, Book book) {
		Book bookToUpdate=getById(id);
		bookToUpdate.setPrice(book.getPrice());
		bookToUpdate.setTitle(book.getTitle());
		bookDao.save(bookToUpdate);
		return bookToUpdate;
	}

	@Override
	public Book deleteBook(int id) {
		Book bookToDelete=getById(id);
		bookDao.delete(bookToDelete);
		return bookToDelete;
	}

	@Override
	public Book getById(int id) {
		return bookDao.findById(id)
				.orElseThrow(() -> new BookNotFoundException("book with id " + id + " is not found"));
	}

}
