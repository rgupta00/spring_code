package com.bookapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.bookapp.dao.Book;
import com.bookapp.service.BookService;

@SpringBootApplication
public class Bookapp1Application implements CommandLineRunner {

	@Autowired
	private BookService bookService;
	
	public static void main(String[] args) {
		SpringApplication.run(Bookapp1Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		//What is this CommandLineRunner?
		//it is a interface an u can override it , it give u a run method 
		//inside run method u can do some useful init job that is run by spring boot
		/*
		 * bookService.addBook(new Book("effective java", 600)); bookService.addBook(new
		 * Book("MEAN in action", 700));
		 * System.out.println("--------book are added--------------");
		 */
		
	}
}
