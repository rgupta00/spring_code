package com.bookapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookapp.dao.Book;
import com.bookapp.service.BookService;

//bookapp/api/book
@RestController
@RequestMapping(path = "api")
public class BookRestController {

	private BookService bookService;

	@Autowired
	public BookRestController(BookService bookService) {
		this.bookService = bookService;
	}
	// , produces = MediaType.APPLICATION_JSON_VALUE

	// -------get all books-------------
	@GetMapping(path = "book")
	public ResponseEntity<List<Book>> getAllBooks() {
		return ResponseEntity.ok(bookService.getAllBooks()); // data + status code
	}

	// -------get an book-------------
	@GetMapping(path = "book/{id}")
	public ResponseEntity<Book> getAnBook(@PathVariable(name = "id") int id) {
		if(1==1)
			throw new NullPointerException();
		
		return ResponseEntity.ok(bookService.getById(id));// data + status code
	}

	// -------post an book-------------
	@PostMapping(path = "book")
	public ResponseEntity<Book> addAnBook(@RequestBody Book book) {
		return ResponseEntity
				.status(HttpStatus.CREATED)
				.body(bookService.addBook(book));
	}


	// -------update an book-------------
	@PutMapping(path = "book/{id}")
	public ResponseEntity<Book> updateAnBook(@PathVariable(name = "id") int id,
			@RequestBody Book book) {
		return ResponseEntity.ok(bookService.updateBook(id, book));
	}
	
	// -------delete an book-------------
	@DeleteMapping(path = "book/{id}")
	public ResponseEntity<Void> deleteAnBook(@PathVariable(name = "id") int id) {
		bookService.deleteBook(id);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
	}
	
}
