package com.demo.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@Entity
@Table(name = "parking_table_one2one")
public class Parking {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int pid;
	private String loc;
	@JoinColumn(name = "eid_fk")
	@OneToOne(cascade = CascadeType.ALL)
	private Employee employee;

	public Parking(String loc) {
		this.loc = loc;
	}
	
	
	
}