package com.bookapp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
@EnableWebSecurity
@Configuration
public class AppSecConfig extends WebSecurityConfigurerAdapter{

	@Autowired
	private DetailsService userDetailsService;

	//authenticaion
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		auth.userDetailsService(userDetailsService);
		
		
		
//		//thre are mulitple way to set user details... hard coded
//		auth.inMemoryAuthentication()
//		.withUser("raj").password("raj123").roles("ADMIN")
//		.and()
//		.withUser("ekta").password("ekta123").roles("MGR")
//		.and()
//		.withUser("gun").password("gun123").roles("EMP");
	}
	
	//Spring sec expect you to use password encoder, 
	//i want to tell spring sec that i dnot want to use password encoder
	@Bean
	PasswordEncoder getPasswordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}

	//authorization policies
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		//CSRF: Cross-Site Request Forgery
		http.csrf().disable()
		.authorizeRequests()
		.antMatchers("/admin/**").hasAnyAuthority("ROLE_ADMIN")
		.antMatchers("/mgr/**").hasAnyAuthority("ROLE_MGR","ROLE_ADMIN")
		.antMatchers("/emp/**").hasAnyAuthority("ROLE_EMP","ROLE_MGR","ROLE_ADMIN")
		.antMatchers("/home/**").permitAll()
		.and()
		.httpBasic()
		.and()
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		//hey spring sec i want not to store password in client side in cookes 
		//i will password with each req
		
		//JWT: JSON web token: authorization ?
		
	}

	
	
}
