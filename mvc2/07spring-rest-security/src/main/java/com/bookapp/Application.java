package com.bookapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.bookapp.dao.User;
import com.bookapp.service.UserService;

@SpringBootApplication
public class Application implements CommandLineRunner{
	
	@Autowired
	private UserService userService;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
//		userService.addUser(new User("raj", "raj123", "ROLE_ADMIN", "raj@gmail.com"));
//		userService.addUser(new User("ekta", "ekta123", "ROLE_MGR", "ekta@gmail.com"));
//		userService.addUser(new User("gun", "gun123", "ROLE_EMP", "gun@gmail.com"));
//		userService.addUser(new User("keshav", "keshav123", "ROLE_EMP", "keshav@gmail.com"));
//		
//		System.out.println("users are added....");
		
	}

}
