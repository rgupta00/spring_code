package com.bookapp.controller;

import java.security.Principal;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MySecureContoller {

	@GetMapping(path="home")
	public String home() {
		return "hello home";
	}
	
	@GetMapping(path="admin")
	public String admin() {
		return "hello admin";
	}
	
	@GetMapping(path="mgr")
	public String mgr(Principal principal) {
		System.out.println(principal.getName());
		return "hello mgr";
	}
	
	@GetMapping(path="emp")
	public String emp() {
		return "hello emp";
	}
}
