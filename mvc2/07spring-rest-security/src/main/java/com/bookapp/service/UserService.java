package com.bookapp.service;
import java.util.*;

import com.bookapp.dao.User;
public interface UserService {
	public void addUser(User user);
	public User findByUsername(String username);
}
