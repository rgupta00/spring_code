package com.bookapp.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookapp.dao.SecUser;
import com.bookapp.service.EmpService;

@RestController
public class MySecureContoller {

	@Autowired
	private EmpService empService;
	
	@GetMapping(path="home")
	public String home() {
		
		return "hello home";
	}
	
	@GetMapping(path="admin")
	public String admin() {
		return "hello admin";
	}
	
	@GetMapping(path="mgr")
	public String mgr(@AuthenticationPrincipal SecUser user) {
//		System.out.println(user.getUsername());
//		System.out.println(user.getPassword());
//		System.out.println("isAccountNonExpired"+user.isAccountNonExpired());
//		System.out.println("isAccountNonLocked"+user.isAccountNonLocked());
		return "hello mgr";
	}
	
	@GetMapping(path="emp")
	public String emp() {
		System.out.println("--------------");
		empService.getAll();
		empService.delete(2);
		System.out.println("--------------------");
		return "hello emp";
	}
}
