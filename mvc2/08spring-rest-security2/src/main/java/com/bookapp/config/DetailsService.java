package com.bookapp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.bookapp.dao.SecUser;
import com.bookapp.dao.User;
import com.bookapp.service.UserService;

@Service
public class DetailsService implements UserDetailsService {

	@Autowired
	private UserService userService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// spring sec will call this method ... inside this method i can call my own
		// service
		// layer and try to get the user based on username passed
		// if username is found then its ok ... otherwise i will throw the excpetion
		User user = userService.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("username is not found");
		}

		// if user is valid spring sec will understand my user,
		// i need to convert my user to the spring security user

//		return new org.springframework.security.core.userdetails.User
//				(user.getUsername(), 
//				user.getPassword(),
//				AuthorityUtils.createAuthorityList(new String[] { user.getRole() }));
		
		return new SecUser(user);

	}

}
