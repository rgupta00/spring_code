package com.bookapp.dao;

import lombok.Data;

@Data
public class Employee {

	private int id;
	private String name;
	private double salary;
	
	
}
