package com.bookapp.dao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
@Entity
@Table(name = "user_table")
@Data
@NoArgsConstructor
public class User {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String username;
	private String password;
	private String role;
	private String eamil;
	
	public User(String username, String password, String role, String eamil) {
		this.username = username;
		this.password = password;
		this.role = role;
		this.eamil = eamil;
	}
	
	
	
}
