package com.bookapp.service;

import java.util.List;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;

import com.bookapp.dao.Employee;
//you can secure you service layer by using lots of method 
/*
 * 1. @secure 
 * 2. jsr 250 annoation
 * 3. PreAuthorized vs PostAu... method 
 * 
 */
public interface EmpService {
	@Secured ({"ROLE_USER", "ROLE_ADMIN","ROLE_MGR"})
	public List<Employee> getAll();
	
	public Employee getEmployeeById(int id);
	
	public Employee save(Employee emp);
	
	
	public Employee update(int empId, Employee emp);
	@PreAuthorize("hasAuthority('ROLE_ADMIN')")
	public Employee delete(int empId);
}
