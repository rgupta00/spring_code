package com.bookapp.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.bookapp.dao.Employee;
@Service
public class EmpServiceImpl implements EmpService {

	@Override
	public List<Employee> getAll() {
		System.out.println("list of all emp");
		return null;
	}

	@Override
	public Employee getEmployeeById(int id) {
		System.out.println("get emp by id");
		return null;
	}

	@Override
	public Employee save(Employee emp) {
		System.out.println("save emp");
		return null;
	}

	@Override
	public Employee update(int empId, Employee emp) {
		System.out.println("update emp");
		return null;
	}

	@Override
	public Employee delete(int empId) {
		System.out.println("delete emp");
		return null;
	}

}
