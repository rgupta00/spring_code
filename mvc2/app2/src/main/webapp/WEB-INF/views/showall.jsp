<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Book store application</title>
</head>
<body>
<body>
	<table>
		<thead>
			<tr>
				<th>book id</th>
				<th>book title</th>
				<th>book price</th>
			</tr>
		</thead>

		<tbody>
			<c:forEach var="b" items="${books}">
				<tr>
					<td>${b.id}</td>
					<td>${b.title}</td>
					<td>${b.price}</td>
				</tr>
			</c:forEach>

		</tbody>
	</table>
	<a href="addBook">Add new Book</a>
</body>

</body>
</html>