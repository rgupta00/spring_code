package com.bookapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
// myapp/hello
@Controller
@RequestMapping(path = "myapp")
public class HelloController {

	
	
	
	//RequestParam: used to get request parameter
	//myapp/hello2?name=raja
	
	//http://localhost:8080/app2/myapp/hello2?name=raja&address=delhi
	@GetMapping(path = "hello2")
	public String hello(@RequestParam(name = "name")String name, 
			@RequestParam(name = "address")String address) {
		
		System.out.println("demo RequestParam");
		System.out.println(name);
		System.out.println(address);
		
		return "abc";
	}
	
	//myapp/hello3/raja/delhi
	
	//Pathvariable: used to pass the path values
	//myapp/hello2?name=raja
	@GetMapping(path = "hello3/{name}/{address}")
	public String hello3(@PathVariable(name = "name")String name,
			@PathVariable(name = "address")String address) {
		System.out.println("demo @PathVariable");
		System.out.println(name);
		System.out.println(address);
		
		return "abc";
	}
	
	
	
	//ModelAndView vs ModelMap	vs Model: same
	
//	@GetMapping(path = "hello2")
//	public String hello(Model map) {
//		map.addAttribute("key", "i love java");
//		return "abc";
//	}
	
	
//	@GetMapping(path = "hello2")
//	public String hello(ModelMap map) {
//		map.addAttribute("key", "i love java");
//		return "abc";
//	}
	
	
	
//	@GetMapping(path = "hello")
//	public ModelAndView hello(ModelAndView mv) {
//		mv.setViewName("abc");
//		mv.addObject("key", "i love java");
//		return mv;
//	}
}
