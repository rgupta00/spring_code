package com.bookapp.controller;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.bookapp.model.dao.Book;
import com.bookapp.model.service.BookService;

@Controller
public class BookController {

	private BookService bookService;
	
	@Autowired
	public BookController(BookService bookService) {
		this.bookService = bookService;
	}
	@PostConstruct
	public void postConsturctMethod() {
//		System.out.println("book is added..");
//		bookService.addBook(new Book("effective java"  , 300));
//		bookService.addBook(new Book("spring in action"  , 600));
		
	}
	@GetMapping(path = "allbooks")
	public ModelAndView getAllBooks(ModelAndView mv) {
		mv.setViewName("showall");
		mv.addObject("books", bookService.getAllBooks());
		return mv;
	}
	
}






