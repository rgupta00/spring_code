package com.bookapp.model.dao;
import java.util.*;
public interface BookDao {
	public List<Book> getAllBooks();
	public Book addBook(Book book);
	public Book updateBook(int id, Book book);
	public Book deleteBook(int id);
	public Book getById(int id);
}
