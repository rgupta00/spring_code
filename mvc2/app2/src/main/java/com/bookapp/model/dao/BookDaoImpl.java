package com.bookapp.model.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;
//hard coded collection...
@Repository
public class BookDaoImpl implements BookDao{
	@Override
	public List<Book> getAllBooks() {
		return Arrays.asList(new Book(121, "head first java", 500),new Book(11, "effective java", 600));
	}

	@Override
	public Book addBook(Book book) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Book updateBook(int id, Book book) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Book deleteBook(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Book getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
