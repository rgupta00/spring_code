package com.bookapp.controller;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.bookapp.model.dao.Book;
import com.bookapp.model.service.BookService;

@Controller
public class BookController {

	private BookService bookService;

	@PostConstruct
	public void addBooks() {
		bookService.addBook(new Book("java", 300));
		bookService.addBook(new Book("spring", 300));
		
	}
	@Autowired
	public BookController(BookService bookService) {
		this.bookService = bookService;
	}
	
	@GetMapping(path = "allbooks")
	public ModelAndView getallbooks(ModelAndView mv) {
		mv.setViewName("showall");
		mv.addObject("books", bookService.getAllBooks());
		return mv;
	}
	
}
