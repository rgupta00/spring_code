package com.bookapp.model.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

@Repository
@Primary
public class BookDaoImpl2 implements BookDao {

	private SessionFactory factroy;

	@Autowired
	public BookDaoImpl2(SessionFactory factroy) {
		this.factroy = factroy;
	}

	private Session getSession() {
		return factroy.getCurrentSession();
	}
	@Override
	public List<Book> getAllBooks() {
		return getSession().createQuery("select b from Book b").getResultList();
	}

	@Override
	public Book addBook(Book book) {
		getSession().save(book);
		return book;
	}

	@Override
	public Book updateBook(int id, Book book) {
		Book bookToUpdate= getById(id);
		bookToUpdate.setPrice(book.getPrice());
		getSession().update(bookToUpdate);
		return bookToUpdate;
	}

	@Override
	public Book deleteBook(int id) {
		Book bookToDelete=getById(id);
		getSession().delete(bookToDelete);
		return bookToDelete;
	}

	@Override
	public Book getById(int id) {
		Book book=getSession().find(Book.class, id);
		if(book==null) {
			throw new BookNotFoundException("book with id :"+ id +" is not found");
		}
		return book;
	}
}
