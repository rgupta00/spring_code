package com.demo.dao;
import java.util.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
@Repository
@Transactional
public interface StudentDao extends JpaRepository<Student, Integer> {
	//how we can write custom method for diff req
	List<Student>findByFirstName(String firstName);
	List<Student>findByFirstNameAndLastName(String firstName, String lastName);
	List<Student>findByFirstNameOrLastName(String firstName, String lastName);
	//List<Student> findByLastNameNotNull(String lastName);
	
	//HQL: JPA
	//find all student whose email is is email
	@Query("select s from Student s where s.email=?1")
	Student getStudentByEmail(String email);
	
	//Named Query
	@Query("select s from Student s where s.email=:emailId")
	Student getStudentByEmailNamedQuery(@Param("emailId")String email);
	
	//native Query
	@Query(value = "select * from stud_table as s where s.email=?1", nativeQuery = true)
	Student getStudentByEmailV2(String email);
	
	//Modfiy the data

	@Modifying
	@Query(value = "update stud_table set first_name=?1 where email=?2", 
	nativeQuery = true)
	void modifyStudentRecord(String firstName, String email);
	
}
