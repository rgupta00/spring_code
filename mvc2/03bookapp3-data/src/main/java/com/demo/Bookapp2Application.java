package com.demo;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.demo.dao.Student;
import com.demo.dao.StudentDao;
@SpringBootApplication
public class Bookapp2Application implements CommandLineRunner {

	@Autowired
	private StudentDao dao;
	
	public static void main(String[] args) {
		SpringApplication.run(Bookapp2Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		/*
		 * // dao.save(new Student("raj", "gupta", "raj@gmail.com")); // dao.save(new
		 * Student("ekta", "kumari", "ekta@gmail.com")); // dao.save(new
		 * Student("kapil", "sharma", "kapil@gmail.com")); // //
		 * System.out.println("---------------");
		 */	
		//
		
//		List<Student>students=dao.findByFirstName("raj");
//		students.forEach(s-> System.out.println(s));
		
//		List<Student>students=dao.findByFirstNameOrLastName("raj", "kumari");
//		students.forEach(s-> System.out.println(s));
		
		Student student=dao.getStudentByEmailNamedQuery("raj@gmail.com");
		System.out.println(student);
		
		//dao.modifyStudentRecord("rajeev","raj@gmail.com");
		
	}

}
