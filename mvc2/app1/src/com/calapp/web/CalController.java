package com.calapp.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.calapp.model.Calculator;

public class CalController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public CalController() { 
    	System.out.println("ctr of servlet is called...");
    }
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, 
	IOException {
		response.setContentType("text/html");
		// req: used to get data from the user
		//resp: used to send some data to the client
		Integer x=Integer.parseInt(request.getParameter("x"));
		Integer y=Integer.parseInt(request.getParameter("y"));
		
		Calculator calculator=new Calculator();
		
		Integer sum=calculator.add(x, y);
		
		//we want that data should be processed by an jsp page
		//for that i need to transfer the control to an jsp page
		request.setAttribute("sum", sum);
		RequestDispatcher rd=request.getRequestDispatcher("result.jsp");
		rd.forward(request, response);
		
	}

}
