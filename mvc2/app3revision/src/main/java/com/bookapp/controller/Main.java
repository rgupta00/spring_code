package com.bookapp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bookapp.model.dao.Book;
import com.bookapp.model.service.BookService;

public class Main {

	private static Logger logger=LoggerFactory.getLogger(Main.class);
	
	public static void main(String[] args) {
		
		ApplicationContext ctx=new ClassPathXmlApplicationContext("beans.xml");
		BookService bookService=ctx.getBean("bookService", BookService.class);
		//bookService.addBook(new Book("thinking big", "abc", 200));
		//bookService.addBook(new Book("spring in action", "abc", 600));
		
		//bookService.getAllBooks().forEach(b-> System.out.println(b));
//		Book book=bookService.getBookById(2);
//		book.setAuthor("pqr");
//		bookService.updateBook(2, book);
		
		//delete the book
		bookService.deleteBook(2);
	}
}
