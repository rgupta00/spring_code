package com.bookapp.model.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

@Repository
@Primary
public class BookDaoImpl2 implements BookDao {

	private SessionFactory factory;
	
	@Autowired
	public BookDaoImpl2(SessionFactory factory) {
		this.factory = factory;
	}
	
	private Session getSession() {
		return factory.getCurrentSession();//session would be open by spring framework
	}

	@Override
	public List<Book> getAllBooks() {
		return getSession().createQuery("select b from Book b").getResultList();
	}

	@Override
	public Book addBook(Book book) {
		getSession().save(book);
		return book;
	}

	@Override
	public Book deleteBook(int id) {
		Book bookToDelete=getBookById(id);
		getSession().delete(bookToDelete);
		return bookToDelete;
	}

	@Override
	public Book updateBook(int id, Book book) {
		Book bookToUpdate=getBookById(id);
		bookToUpdate.setTitle(book.getTitle());
		bookToUpdate.setPrice(book.getPrice());
		bookToUpdate.setAuthor(book.getAuthor());
		getSession().update(bookToUpdate);
		return bookToUpdate;
		
	}

	@Override
	public Book getBookById(int id) {
		Book book=getSession().find(Book.class, id);
		if(book==null)
			throw new BookNotFoundException("book with id :"+ id +" is not found");
		return book;
	}

}
