package com.demo.controller;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.dao.DepartmentRepo;
import com.demo.entities.Department;

@RestController
public class DeptRestController {

	@Autowired
	private DepartmentRepo deptRepo;
	
	@GetMapping(path = "department")
	public List<Department> getAllDept(){
		return deptRepo.findAll();
	}
}
