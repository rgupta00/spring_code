package com.bookapp.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.bookapp.dao.Book;
import com.bookapp.dao.BookDao;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BookServiceTest {

	@Autowired
	private BookService bookService;
	
	@MockBean
	private BookDao bookDao;
	
	@Test
	public void getAllBookTest() {
		
		when(bookDao.findAll()).thenReturn(Arrays.asList(new Book(1, "java", 200),new Book(1, "java", 200)));
		assertEquals(2	, bookService.getAllBooks().size());
		
	}
	
}
