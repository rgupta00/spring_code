package com.bookapp.dao;
import java.util.*;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class BookDaoTest {
	@Autowired
	private BookDao bookDao;
	@Test
	@Rollback(value = false)
	@Order(1)
	public void addBookTest() {
		Book book=new Book("test in action", 300);
		bookDao.save(book);
		Assertions.assertThat(book.getId()).isGreaterThan(0);
	}
	@Rollback(value = false)
	@Order(2)
	@Test
	public void getByIdTest() {
		Book book=bookDao.findById(1).get();
		Assertions.assertThat(book.getId()).isEqualTo(1);
	}
	
	@Rollback(value = false)
	@Order(3)
	@Test
	public void getAllEmployeeTest() {
		List<Book>books=bookDao.findAll();
		Assertions.assertThat(books.size()).isGreaterThan(0);
	}
	
	@Rollback(value = false)
	@Order(4)
	@Test
	public void updateEmployeeTest() {
		Book book=bookDao.findById(1).get();
		book.setTitle("spring test");
		bookDao.save(book);
		Assertions.assertThat(book.getTitle()).isEqualTo("spring test");
	}
	
	@Rollback(value = false)
	@Order(5)
	@Test
	public void deleteEmployeeTest() {
		Book book=bookDao.findById(1).get();
		bookDao.delete(book);
		Optional<Book> bookOpt=bookDao.findById(1);
		Book book2=null;
		if(bookOpt.isPresent()) {
			book2=bookOpt.get();
		}
		Assertions.assertThat(book2).isNull();
	}
}
