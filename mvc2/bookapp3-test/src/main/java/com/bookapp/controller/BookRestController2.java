package com.bookapp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bookapp.dao.Book;
import com.bookapp.service.BookService;


//REsponesEntity= data + http status code

@RestController
public class BookRestController2 {

	private BookService bookService;

	@Autowired
	public BookRestController2(BookService bookService) {
		this.bookService = bookService;
	}

	// ------getting all books----------
	@GetMapping(path = "book")
	public ResponseEntity<List<Book>> allbooks() {
		return ResponseEntity.ok().body(bookService.getAllBooks());
	}

	// ------getting an book----------
	@GetMapping(path = "book/{id}")
	public ResponseEntity<Book> getAnBook(@PathVariable(name = "id") int id) {
		return ResponseEntity.ok(bookService.getById(id));
	}

	// ------post an new book----------
	@PostMapping(path = "book")
	public ResponseEntity<Book> postAnBook(@Valid @RequestBody Book book) {
		return ResponseEntity.status(HttpStatus.CREATED).body(bookService.addBook(book));
	}

	// ------update an existing book----------
	@PutMapping(path = "book/{id}")
	public ResponseEntity<Book> updateAnBook(@PathVariable(name = "id") int id, @RequestBody Book book) {
		return ResponseEntity.ok(bookService.updateBook(id, book));
	}

	// ------getting an book----------
	@DeleteMapping(path = "book/{id}")
	public ResponseEntity<Void> deleteAnBook(@PathVariable(name = "id") int id) {
		bookService.deleteBook(id);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
	}
}
