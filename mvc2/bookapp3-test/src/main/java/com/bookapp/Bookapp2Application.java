package com.bookapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

//@EnableScheduling
//@EnableCaching
@SpringBootApplication
public class Bookapp2Application {

	public static void main(String[] args) {
		SpringApplication.run(Bookapp2Application.class, args);
	}

	@Bean
	public CacheManager getCacheManager() {
		
		//"books" is the name of cache
		ConcurrentMapCacheManager cacheManager=new ConcurrentMapCacheManager("books");
		return cacheManager;
		
	}
}
