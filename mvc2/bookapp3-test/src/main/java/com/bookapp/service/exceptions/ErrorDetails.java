package com.bookapp.service.exceptions;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErrorDetails {
	private String message;
	private String errorDetail;
	private String email;
	private Date date;
	
	//project lombok
}
