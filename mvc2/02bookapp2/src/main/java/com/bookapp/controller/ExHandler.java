package com.bookapp.controller;

import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.bookapp.service.exceptions.BookNotFoundException;
import com.bookapp.service.exceptions.ErrorDetails;
@ControllerAdvice //throws advice of aop
@RestController
public class ExHandler  extends ResponseEntityExceptionHandler{

	//catch all...
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorDetails> handleAllError(Exception ex, WebRequest request){
		ErrorDetails ed=new ErrorDetails();
		ed.setEmail("rupta.mtech@gamil.com");
		ed.setDate(new Date());
		ed.setMessage("some server side error");
		ed.setErrorDetail(request.getDescription(false));
		
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ed);
		
	}

	
	@ExceptionHandler(BookNotFoundException.class)
	public ResponseEntity<ErrorDetails> handle404Error(Exception ex, WebRequest request){
		ErrorDetails ed=new ErrorDetails();
		ed.setEmail("rupta.mtech@gamil.com");
		ed.setDate(new Date());
		ed.setMessage(ex.getMessage());
		ed.setErrorDetail(request.getDescription(false));
		
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ed);
		
	}


	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid
	(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ErrorDetails ed=new ErrorDetails();
		ed.setEmail("rupta.mtech@gamil.com");
		ed.setDate(new Date());
		ed.setMessage(ex.getMessage());
		ed.setErrorDetail(request.getDescription(false));
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ed);
	}
	
	
}
