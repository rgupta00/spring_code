package com.bookapp.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.bookapp.dao.Customer;

@RestController
public class CustomerRestController {

	// ------getting an an customer----------
	@GetMapping(path = "customer/{id}")
	public Customer getAnCustomer(@PathVariable(name = "id") int id) {
		return new Customer(id, "raj", "gupta", "delhi","raj4354545");
	}
}
