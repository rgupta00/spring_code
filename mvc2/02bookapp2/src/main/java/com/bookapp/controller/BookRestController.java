//package com.bookapp.controller;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.bookapp.dao.Book;
//import com.bookapp.service.BookService;
//
//@RestController
//public class BookRestController {
//
//	private BookService bookService;
//
//	@Autowired
//	public BookRestController(BookService bookService) {
//		this.bookService = bookService;
//	}
//
//	// ------getting all books----------
//	@GetMapping(path = "book")
//	public List<Book> allbooks() {
//		return bookService.getAllBooks();
//	}
//
//	// ------getting an book----------
//	@GetMapping(path = "book/{id}")
//	public Book getAnBook(@PathVariable(name = "id") int id) {
//		return bookService.getById(id);
//	}
//
//	// ------post an new book----------
//	@PostMapping(path = "book")
//	public Book postAnBook(@RequestBody Book book) {
//		return bookService.addBook(book);
//	}
//
//	// ------update an existing book----------
//	@PutMapping(path = "book/{id}")
//	public Book updateAnBook(@PathVariable(name = "id") int id, @RequestBody Book book) {
//		return bookService.updateBook(id, book);
//	}
//
//	// ------getting an book----------
//	@DeleteMapping(path = "book/{id}")
//	public Book deleteAnBook(@PathVariable(name = "id") int id) {
//		return bookService.deleteBook(id);
//	}
//}
