package com.bookapp.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
//@JsonIgnore : filtering the data , let say dont want to show some data in json resp
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Customer {

	private int id;
	private String fName;
	private String lName;
	private String address;
	@JsonIgnore
	private String password;
}
