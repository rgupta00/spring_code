package com.bookapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class ScheduledJob {

	private Logger logger=LoggerFactory.getLogger(ScheduledJob.class);
	
	@Autowired
	private BookService bookService;
	
	@Scheduled(initialDelay = 500, fixedRate = 1800000)
	public void doPeriodically() {
		logger.info("method is called doPeriodically....");
		bookService.evictAllCache();
	}
}
