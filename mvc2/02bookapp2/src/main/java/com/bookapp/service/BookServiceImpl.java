package com.bookapp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bookapp.dao.Book;
import com.bookapp.dao.BookDao;

import com.bookapp.service.exceptions.BookNotFoundException;


@Service
@Transactional
public class BookServiceImpl implements BookService {

	private BookDao bookDao;

	@Autowired
	public BookServiceImpl(BookDao bookDao) {
		this.bookDao = bookDao;
	}

	@Override
	public List<Book> getAllBooks() {
		return bookDao.findAll();
	}

	// this method will return an object , hey cahcing please put that object into 
	//cache with the key of the id whose object is reture
	@CachePut(value = "books", key = "#result.id")
	@Override
	public Book addBook(Book book) {
		bookDao.save(book);
		return book;
	}
	@CachePut(value = "books", key = "#result.id")
	@Override
	public Book updateBook(int id, Book book) {
		Book bookToUpdate=getById(id);
		bookToUpdate.setPrice(book.getPrice());
		bookToUpdate.setTitle(book.getTitle());
		bookDao.save(bookToUpdate);
		return bookToUpdate;
	}
	@CacheEvict(value = "books", key = "#id")
	@Override
	public Book deleteBook(int id) {
		Book bookToDelete=getById(id);
		bookDao.delete(bookToDelete);
		return bookToDelete;
	}

	@Cacheable(value = "books", key = "#id")
	@Override
	public Book getById(int id) {
		return bookDao.findById(id)
				.orElseThrow(() -> new BookNotFoundException("book with id " + id + " is not found"));
	}

	//i need to apply some trick so that this method can be called periodically
	//lets says each after 20 min
	
	@CacheEvict(value = "books", allEntries =true)
	@Override
	public void evictAllCache() {}

}
