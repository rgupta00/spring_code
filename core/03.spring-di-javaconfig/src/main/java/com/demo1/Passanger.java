package com.demo1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component(value = "passanger")
public class Passanger {
	@Value(value = "raja")
	private String name;

	private Vehical vehical;
	
	public Passanger() {
	}
	
	@Autowired
	public Passanger(Vehical vehical) {
		this.vehical = vehical;
	}
	public Passanger(String name, Vehical vehical) {
		System.out.println("ctr injection is working");
		this.name = name;
		this.vehical = vehical;
	}



	public void travel() {
		System.out.println("name : "+ name);
		vehical.move();
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setVehical(Vehical vehical) {
		this.vehical = vehical;
	}
}
