package com.bankapp.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AccountDaoImplJdbc implements AccountDao{
	
	private Map<Integer, Account> accountsMap=new HashMap<Integer, Account>();
	
	public AccountDaoImplJdbc() {
		accountsMap.put(1, new Account(1, "raj", 1000));
		accountsMap.put(2, new Account(2, "ekta", 1000));
	}
	
	@Override
	public List<Account> getAllAccounts() {
		System.out.println("using jdbc");
		return new ArrayList<Account>(accountsMap.values());
	}

	@Override
	public void updateAccount(Account account) {
		
		accountsMap.put(account.getId(), account);
	}

	@Override
	public Account getById(int id) {
		return accountsMap.get(id);
	}
}

