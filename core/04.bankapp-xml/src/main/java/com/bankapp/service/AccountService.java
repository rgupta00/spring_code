package com.bankapp.service;

import java.util.List;

import com.bankapp.dao.Account;

public interface AccountService {
	public List<Account> getAllAccounts();
	public void fundTransfer(int fromId, int toId, double amount);
	public Account getById(int id);
}
