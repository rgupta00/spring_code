package com.customer.controller;

import java.util.Date;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.customer.config.AppConfig;
import com.customer.dao.Customer;
import com.customer.service.CustomerService;

public class Main {

	public static void main(String[] args) {
	
		//ApplicationContext ctx=new ClassPathXmlApplicationContext("beans.xml");
		AnnotationConfigApplicationContext ctx=
				new AnnotationConfigApplicationContext(AppConfig.class);
		
		CustomerService customerService=ctx.getBean("customerService", CustomerService.class);
		
		Customer customer=new Customer("sumit", "delhi", "35454545", "anil@gmail.com", new Date(), 1);
		
		customerService.addCustomer(customer);
		
	}
}

