package com.customer.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan(basePackages = { "com.customer" })
@EnableTransactionManagement // <tx:annotation-driven transaction-manager="transactionManager"/>
@PropertySource(value = "classpath:db.properties")
public class AppConfig {
	@Value(value = "${jdbc.url}")
	private String url;

	@Value(value = "${jdbc.password}")
	private String password;

	@Value(value = "${jdbc.username}")
	private String username;

	@Value(value = "${jdbc.driverName}")
	private String driverName;

	@Bean
	public DataSource getDataSource() {
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setUrl(url);
		ds.setUsername(username);
		ds.setPassword(password);
		ds.setDriverClassName(driverName);
		return ds;
	}

	@Bean
	@Autowired
	public LocalSessionFactoryBean getSessionFactory(DataSource ds) {
		LocalSessionFactoryBean sf = new LocalSessionFactoryBean();
		sf.setDataSource(ds);
		sf.setPackagesToScan(new String[] { "com.customer.dao" });
		sf.setHibernateProperties(getHibernateProperties());
		return sf;
	}

	private Properties getHibernateProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.hbm2ddl.auto", "update");
		properties.setProperty("hibernate.show_sql", "true");
		properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL57Dialect");
		properties.setProperty("hibernate.format_sql", "true");
		return properties;
	}
	
	@Bean
	public HibernateTransactionManager getHibernateTransactionManager(SessionFactory factory) {
		HibernateTransactionManager txMgr=new HibernateTransactionManager();
		txMgr.setSessionFactory(factory);
		return txMgr;
	}
}
