package com.demo;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class Audiance {
	
	//Around advice : can act as a sec guard, spring sec use it internally...
	@Around("execution(public String doMagic())")
	public Object aroudDemo(ProceedingJoinPoint pjp) throws Throwable {
		System.out.println("magic start hone wala hey..");
		Object value= pjp.proceed();
		System.out.println("wow, maza aa gaya....");
		return value;
	}
	
	
	
//	//@Before("execution(public void doMagic())")
//	//@After("execution(public void doMagic())")
//	@AfterReturning("execution(public void doMagic())")
//	public void clapping() {
//		System.out.println("wow, maza aa gaya....");
//	}
//	@AfterThrowing("execution(public void doMagic())")
//	public void callDr() {
//		System.out.println("call Dr now !");
//	}
}
