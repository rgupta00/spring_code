package com.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DemoLogging {
	
	private static Logger logger= LoggerFactory.getLogger(DemoLogging.class);
	
	public static void main(String[] args) {
		
		
		logger.info("this code is using logging");
		logger.info("this is another log message");
		
		String data ="121A";
		try {
		Integer val=Integer.parseInt(data);
		}catch(Exception e) {
			logger.error("ex occured:"+e);
		}
		
		
	}

}
