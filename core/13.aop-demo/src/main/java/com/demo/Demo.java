package com.demo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo {

	public static void main(String[] args) {
		
		AnnotationConfigApplicationContext ctx=
				new AnnotationConfigApplicationContext(AppConfig.class);
		
//		ApplicationContext ctx=new ClassPathXmlApplicationContext("demo.xml");
		
		Magician magician=ctx.getBean("magician", Magician.class);
		
		String value= magician.doMagic();
		System.out.println(value);
	}
}
