package com.customer.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerDaoImpl implements CustomerDao {

	private SessionFactory factory;

	@Autowired
	public CustomerDaoImpl(SessionFactory factory) {
		this.factory = factory;
	}

	private Session getSession() {
		return factory.getCurrentSession();
	}

	@Override
	public List<Customer> getAllCustomers() {
		return getSession().createQuery("select c from Customer c").getResultList();
	}

	@Override
	public Customer addCustomer(Customer customer) {
		getSession().save(customer);
		return customer;
	}

	@Override
	public Customer updateCustomer(int id, Customer customer) {

		Customer customerInDb = null;

		customerInDb = getById(id);
		customerInDb.setAddress(customer.getAddress());
		customerInDb.setEmail(customer.getEmail());
		customerInDb.setPhone(customer.getPhone());

		getSession().update(customerInDb);

		return customerInDb;
	}

	@Override
	public Customer deleteCustomer(int id) {
		Customer customerInDb = getById(id);
		getSession().delete(customerInDb);
		return customerInDb;
	}

	@Override
	public Customer getById(int id) {
		
		Customer customer = getSession().get(Customer.class, id);
		if (customer == null) {
			throw new CustomerNotFoundException("customer with id :" + id + " is not found");
		}
		return customer;
	}

	@Override
	public Customer getByName(String name) {
		Customer customer = (Customer) getSession().createQuery("select c from Customer c where c.name=:cname")
				.setParameter("cname", name).getSingleResult();
		if (customer == null)
			throw new CustomerNotFoundException("customer with name " + name + " is not found");
		return customer;
	}
}
