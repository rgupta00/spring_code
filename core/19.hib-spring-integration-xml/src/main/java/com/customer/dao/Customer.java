package com.customer.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
@Entity
@Table(name = "cust_table2")
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "customer_id", nullable = false)
	private int id;
	
	@Column(name = "customer_name", nullable = false)
	private String name;
	
	@Column(name = "customer_address", nullable = false)
	private String address;
	
	@Column(name = "customer_phone", nullable = false)
	private String phone;
	
	@Column(name = "customer_email", nullable = false)
	private String email;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "customer_dob", nullable = false)
	private Date dob;
	
	@Column(name = "customer_rank", nullable = false)
	private int rank;
	
	@Transient// transient means it should not store to db
	@Column(name = "customer_no_of_kids")
	private int noOfKids;
	
	public int getNoOfKids() {
		return noOfKids;
	}

	public void setNoOfKids(int noOfKids) {
		this.noOfKids = noOfKids;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public Customer() {
	}

	public Customer( String name, String address, String phone, String email, Date dob,int rank ) {
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.email = email;
		this.dob = dob;
		this.rank=rank;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name + ", address=" + address + ", phone=" + phone + ", email="
				+ email + ", dob=" + dob + ", rank=" + rank + ", noOfKids=" + noOfKids + "]";
	}

	
	
}
