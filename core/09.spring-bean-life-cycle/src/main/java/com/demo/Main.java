package com.demo;

import javax.annotation.PreDestroy;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {
		AbstractApplicationContext ctx=new ClassPathXmlApplicationContext("demo.xml");
		ctx.registerShutdownHook(); //this step is req if u want to @PreDestroy
		
		
		Account account=ctx.getBean("account",Account.class);
		
		System.out.println(account);
		
	}
}
