package com.demo;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class Account {
	private int id;
	private String name;
	private double balance;
	
	//PostConstruct: 3 ways
	
	
	@PostConstruct
	public void myPostConstruct() {
		
	}
	
	@PreDestroy
	public void preDestroy() {
		
	}
	
	//Pre destroy: 3 ways: constructor
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public Account(int id, String name, double balance) {
		this.id = id;
		this.name = name;
		this.balance = balance;
	}
	public Account() {
		
	}
	@Override
	public String toString() {
		return "Account [id=" + id + ", name=" + name + ", balance=" + balance + "]";
	}
	
	
	
}
