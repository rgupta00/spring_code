package com.bookapp.controller;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bookapp.dao.Book;
import com.bookapp.service.BookService;

public class DemoMain {

	public static void main(String[] args) {
		ApplicationContext ctx=new ClassPathXmlApplicationContext("demo.xml");
		
		BookService bookService=ctx.getBean("bookService", BookService.class);
		bookService.addBook(new Book(12, "java in action"));
		
	}
}
