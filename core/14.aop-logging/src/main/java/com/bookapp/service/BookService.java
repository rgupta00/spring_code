package com.bookapp.service;

import com.bookapp.dao.Book;

public interface BookService {
	public void addBook(Book book);
}
