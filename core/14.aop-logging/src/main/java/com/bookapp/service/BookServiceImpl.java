package com.bookapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookapp.dao.Book;
import com.bookapp.dao.BookDao;
import com.bookapp.dao.BookDaoImpl;

//service layer : BL + cross cutting concern 
//Mgr : addBook logging
@Service(value = "bookService")
public class BookServiceImpl implements BookService {

	private BookDao dao;
	

	@Autowired
	public BookServiceImpl(BookDao dao) {
		this.dao = dao;
	}

	public void addBook(Book book) {	
		dao.addBook(book);
	}
}
