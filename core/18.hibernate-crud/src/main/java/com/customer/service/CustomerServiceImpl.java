package com.customer.service;

import java.util.List;

import com.customer.dao.Customer;
import com.customer.dao.CustomerDao;
import com.customer.dao.CustomerDaoImpl;

public class CustomerServiceImpl implements CustomerService {

	private CustomerDao customerDao;
	

	public CustomerServiceImpl() {
		customerDao=new CustomerDaoImpl();
	}

	@Override
	public List<Customer> getAllCustomers() {
		return customerDao.getAllCustomers();
	}

	@Override
	public Customer addCustomer(Customer customer) {
		return customerDao.addCustomer(customer);
	}

	@Override
	public Customer updateCustomer(int id, Customer customer) {
		return customerDao.updateCustomer(id, customer);
	}

	@Override
	public Customer deleteCustomer(int id) {
		return customerDao.deleteCustomer(id);
	}

	@Override
	public Customer getById(int id) {
		return customerDao.getById(id);
	}

	@Override
	public Customer getByName(String name) {
		return customerDao.getByName(name);
	}

}
