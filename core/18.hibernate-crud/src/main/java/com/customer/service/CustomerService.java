package com.customer.service;

import java.util.List;

import com.customer.dao.Customer;

public interface CustomerService {
	public List<Customer> getAllCustomers();
	public Customer addCustomer(Customer customer);
	public Customer updateCustomer(int id, Customer customer);
	public Customer deleteCustomer(int id);
	public Customer getById(int id);
	public Customer getByName(String name);
}
