package com.customer.dao;
import java.util.*;
public interface CustomerDao {
	public List<Customer> getAllCustomers();
	public Customer addCustomer(Customer customer);
	public Customer updateCustomer(int id, Customer customer);
	public Customer deleteCustomer(int id);
	public Customer getById(int id);
	public Customer getByName(String name);
}
