package com.customer.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class CustomerDaoImpl implements CustomerDao {

	private SessionFactory factory;

	public CustomerDaoImpl() {
		factory = HibernateFactory.getSessionFactory();
	}

	@Override
	public List<Customer> getAllCustomers() {
		return factory.openSession().createQuery("select c from Customer c").getResultList();
	}

	//Spring + hib : spring aop: declartive tx
	@Override
	public Customer addCustomer(Customer customer) {
		Session session = factory.openSession();
		Transaction tx = session.getTransaction();
		try {
			tx.begin();
			session.save(customer);
			tx.commit();
		} catch (HibernateException ex) {
			ex.printStackTrace();
			tx.rollback();
		}
		return customer;
	}

	@Override
	public Customer updateCustomer(int id, Customer customer) {
		Session session = factory.openSession();
		Transaction tx = session.getTransaction();
		Customer customerInDb = null;
		try {
			tx.begin();
			customerInDb = getById(id);
			customerInDb.setAddress(customer.getAddress());
			customerInDb.setEmail(customer.getEmail());
			customerInDb.setPhone(customer.getPhone());

			session.update(customerInDb);

			tx.commit();
		} catch (HibernateException ex) {
			ex.printStackTrace();
			tx.rollback();
		}
		return customerInDb;
	}

	@Override
	public Customer deleteCustomer(int id) {
		Session session = factory.openSession();
		Transaction tx = session.getTransaction();
		Customer customerInDb = null;
		try {
			tx.begin();
			customerInDb = getById(id);

			session.delete(customerInDb);

			tx.commit();
		} catch (HibernateException ex) {
			ex.printStackTrace();
			tx.rollback();
		}
		return customerInDb;
	}

	@Override
	public Customer getById(int id) {
		Session session = factory.openSession();
		Customer customer = session.get(Customer.class, id);
		if (customer == null) {
			throw new CustomerNotFoundException("customer with id :" + id + " is not found");
		}
		session.close();
		return customer;
	}

	@Override
	public Customer getByName(String name) {
		Customer customer = (Customer) factory.openSession().createQuery("select c from Customer c where c.name=:cname")
				.setParameter("cname", name).getSingleResult();
		if (customer == null)
			throw new CustomerNotFoundException("customer with name " + name + " is not found");
		return customer;
	}

}
