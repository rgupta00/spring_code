package com.customer.controller;

import java.util.Date;
import java.util.*;
import com.customer.dao.Customer;
import com.customer.service.CustomerService;
import com.customer.service.CustomerServiceImpl;

public class Main {

	public static void main(String[] args) {
	
		CustomerService customerService=
				new CustomerServiceImpl();
		
		//add 
		
//		Customer customer=new Customer("ravi", "delhi", "5454545", "ravi@gmail.com", new Date(),1);
//		Customer customer2=new Customer("tarun", "banglore", "8809809", "tarun@gmail.com", new Date(),2);
//		customerService.addCustomer(customer2);
//		customerService.addCustomer(customer);
		
		//update
//		Customer customerToUpdate=customerService.getById(2);
//		customerToUpdate.setAddress("chennai");
//		customerService.updateCustomer(2, customerToUpdate);
		
		//print all customers
		
		//List<Customer> customers=customerService.getAllCustomers();
		//customers.forEach(c-> System.out.println(c));
		
		//get customer by name
		//Customer customer=customerService.getByName("indu");
		//System.out.println(customer);
		
		//delete operation
		Customer customer=customerService.deleteCustomer(2);
		System.out.println("delted customer rec");
		
	}
}

