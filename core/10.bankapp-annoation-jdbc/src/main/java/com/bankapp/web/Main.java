package com.bankapp.web;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bankapp.service.AccountService;

public class Main {
	public static void main(String[] args) {
		
		ApplicationContext ctx=new ClassPathXmlApplicationContext("bank.xml");
		
		AccountService accountService=ctx.getBean("accountservice", AccountService.class);
		
		accountService.getAllAccounts().forEach(a-> System.out.println(a));
		
//		accountService.fundTransfer(1, 2, 100);
//		
//		System.out.println("-----after fund transfer ------------");
//		
//		accountService.getAllAccounts().forEach(a-> System.out.println(a));
		
	}
}
