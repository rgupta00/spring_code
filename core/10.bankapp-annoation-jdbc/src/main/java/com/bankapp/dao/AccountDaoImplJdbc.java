package com.bankapp.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
@Repository
@Primary
public class AccountDaoImplJdbc implements AccountDao{
	
	private DataSource dataSource;
	
	@Autowired
	public AccountDaoImplJdbc(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public List<Account> getAllAccounts() {
		List<Account> accounts=new ArrayList<Account>();
		Account account=null;
		//jdbc code to get all the records from the database
		Connection connection;
		try {
			connection = dataSource.getConnection();
			PreparedStatement pstmt=connection.prepareStatement("select * from account");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				account=new Account(rs.getInt(1), rs.getString(2), rs.getDouble(3));
				accounts.add(account);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			//close the connetion :(
		}
		
		return accounts;
	}

	@Override
	public void updateAccount(Account account) {
		
	}

	@Override
	public Account getById(int id) {
		return null;
	}
}

