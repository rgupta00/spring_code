package com.demo;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class UpdateCustomer {
	public static void main(String[] args) {
		//1. SessionFactroy
		SessionFactory factory=HibernateFactory.getSessionFactory();
		
		//2. obtain session 
		Session session=factory.openSession();
		Transaction tx= session.getTransaction();
		
		try {
			tx.begin();
				//get the record to update
			Customer customer=session.get(Customer.class, 2);
			if(customer!=null) {
				customer.setName("rajesh");
			}else
			{
				System.out.println("customer is not found");
			}
			
			tx.commit();
			
		}catch(HibernateException ex) {
			ex.printStackTrace();
			tx.rollback();
		}
		
		session.close();
		factory.close();
	}
}
