package com.demo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

public class AddCustomer {

	public static void main(String[] args) throws ParseException {
		//hibernate :ORM
		//framework to intract with database
		
		//SessionFactory vs Session
		
		//1. first i need to create the object of session factory
		SessionFactory factory=HibernateFactory.getSessionFactory();
		
		//2. create Session from sessionfactroy
		
		Session session=factory.openSession();
		
		//3. you need to start transaction
		
		session.getTransaction().begin();
		
		SimpleDateFormat fmt=new SimpleDateFormat("dd/MM/yyyy");
		Date dobDate=fmt.parse("11/11/2011");
		
		Customer customer=new Customer("nitin", "delhi", "54545454", "nitin@gamil.com", dobDate);
		customer.setRank("rank 1");
		customer.setNoOfKids(2);
		//hibernte help you to interact with database in object oriented way
		
		session.save(customer);
		
		session.getTransaction().commit();
		
		
		session.close();
		factory.close();
		
		
		
	}
}
