package com.demo;
import java.util.*;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class ShowAll {

	public static void main(String[] args) {
		SessionFactory factory=HibernateFactory.getSessionFactory();
		
		Session session=factory.openSession();
		//OO Way SQL vs HQL (OO way of writing SQL)
		//select * from customer
		//select c from Customer c
		//from Customer
		List<Customer> customers=session.createQuery("select c from Customer c")
				.getResultList();
		
		customers.forEach(c-> System.out.println(c));
		
		session.close();
		
		factory.close();
	}
}
