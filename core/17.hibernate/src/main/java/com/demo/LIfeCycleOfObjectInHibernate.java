package com.demo;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;



public class LIfeCycleOfObjectInHibernate {

	public static void main(String[] args) {
		//Transient --- persisted --- detached
		
		
		SessionFactory factory=HibernateFactory.getSessionFactory();
		
		Session session=factory.openSession();
		
		session.getTransaction().begin();
		Customer customer=session.get(Customer.class, 1);//object directly goes to persited sate
		
		//dirty checking ?
		customer.setAddress("NY");
		session.getTransaction().commit();
		System.out.println(customer);
		//System.out.println(customer2);
		session.close();
		
		factory.close();
		
		
		
		
//		//Transient: the hibernate dont know about this object
//		Customer customer=new Customer("pawan", "delhi", "54355454", "p@gmail.com", new Date());
//		
//		
//		session.getTransaction().begin();
//		session.save(customer);
//		//persisted: after save method object goes into persited state
//		session.getTransaction().commit();
		
		
		
	}
}
