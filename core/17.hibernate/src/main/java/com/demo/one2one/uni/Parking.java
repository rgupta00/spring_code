package com.demo.one2one.uni;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

//Paring ----Emp

@Entity
@Table(name = "p_table_one2one")
public class Parking {
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int pid;
	private String loc;
	
	@OneToOne
	private Emp emp;
	
	public Parking() {}


	public Parking(String loc) {
		this.loc = loc;
	}

	

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getLoc() {
		return loc;
	}

	public void setLoc(String loc) {
		this.loc = loc;
	}

	public Emp getEmp() {
		return emp;
	}

	public void setEmp(Emp emp) {
		this.emp = emp;
	}

	@Override
	public String toString() {
		return "Parking [pid=" + pid + ", loc=" + loc + ", emp=" + emp + "]";
	}
	
	
}
