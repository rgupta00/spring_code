package com.demo.one2one.uni;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.demo.Customer;
import com.demo.HibernateFactory;

public class Main {

	public static void main(String[] args) {
		SessionFactory factory = HibernateFactory.getSessionFactory();

		Session session = factory.openSession();

		session.getTransaction().begin();

		Emp e1=new Emp("ravi");
		Parking p1=new Parking("A21");
		p1.setEmp(e1);
		
		Emp e2=new Emp("anil");
		Parking p2=new Parking("A12");
		p2.setEmp(e2);
		
		session.save(e1);
		session.save(e2);
		session.save(p1);
		session.save(p2);
		
		session.getTransaction().commit();

		session.close();
		factory.close();
	}
}
