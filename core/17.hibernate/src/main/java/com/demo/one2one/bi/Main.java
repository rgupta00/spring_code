package com.demo.one2one.bi;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.demo.Customer;
import com.demo.HibernateFactory;

public class Main {

	public static void main(String[] args) {
		SessionFactory factory = HibernateFactory.getSessionFactory();

		Session session = factory.openSession();

		session.getTransaction().begin();

		Emp e1=new Emp("ravi");
		Emp e2=new Emp("anil");
		
		
		Parking p1=new Parking("A21");
		Parking p2=new Parking("A12");
		
		e1.setParking(p1);
		e2.setParking(p2);
		
		p1.setEmp(e1);
		p2.setEmp(e2);
		
		//session.save(e1);
		//session.save(e2);
		session.save(p1);
		session.save(p2);
		
		session.getTransaction().commit();

		session.close();
		factory.close();
	}
}
