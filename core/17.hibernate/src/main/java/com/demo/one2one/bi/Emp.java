package com.demo.one2one.bi;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "e_table")
public class Emp {
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int eid;
	private String ename;
	
	@OneToOne(mappedBy = "emp")
	private Parking parking;
	
	
	
	public Parking getParking() {
		return parking;
	}
	public void setParking(Parking parking) {
		this.parking = parking;
	}
	public int getEid() {
		return eid;
	}
	public void setEid(int eid) {
		this.eid = eid;
	}
	public String getEname() {
		return ename;
	}
	public void setEname(String ename) {
		this.ename = ename;
	}
	public Emp() {}
	
	public Emp( String ename) {
		this.ename = ename;
	}
	@Override
	public String toString() {
		return "Emp [eid=" + eid + ", ename=" + ename + "]";
	}
	
	
	
}
