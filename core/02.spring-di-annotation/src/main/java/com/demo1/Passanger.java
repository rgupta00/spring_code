package com.demo1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component(value = "passanger")
public class Passanger {
	@Value(value = "raja")
	private String name;
	/*
	 * 1. field injection
	 * 2. setter
	 * 3. ctr injection
	 */
	//@Autowired	//field injection
	private Vehical vehical;
	
	public Passanger() {
	}
	
	
	@Autowired
	public Passanger(Vehical vehical) {
		this.vehical = vehical;
	}



	//ctr injection
	public Passanger(String name, Vehical vehical) {
		System.out.println("ctr injection is working");
		this.name = name;
		this.vehical = vehical;
	}



	public void travel() {
		System.out.println("name : "+ name);
		vehical.move();
	}

	//i want that this setters must be called spring framework " pull vs push"
	public void setName(String name) {
		this.name = name;
	}

	//@Autowired
	public void setVehical(Vehical vehical) {
		this.vehical = vehical;
	}
}
