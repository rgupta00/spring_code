package com.demo1;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

//<bean id="vehical1" class="com.demo1.Bike" primary="true"/>
@Component
public class Bike implements Vehical {

	@Override
	public void move() {
		System.out.println("moving on a bike");
	}

}
