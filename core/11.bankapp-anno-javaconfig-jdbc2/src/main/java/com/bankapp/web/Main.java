package com.bankapp.web;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bankapp.config.AppConfiguration;
import com.bankapp.config.AppConfiguration2;
import com.bankapp.service.AccountService;

public class Main {
	public static void main(String[] args) {
		
		AnnotationConfigApplicationContext ctx=
				new AnnotationConfigApplicationContext(AppConfiguration2.class);
		
		AccountService accountService=ctx.getBean("accountservice", AccountService.class);
		
		accountService.getAllAccounts().forEach(a-> System.out.println(a));
		
//		accountService.fundTransfer(1, 2, 100);
//		
//		System.out.println("-----after fund transfer ------------");
//		
//		accountService.getAllAccounts().forEach(a-> System.out.println(a));
		
	}
}
