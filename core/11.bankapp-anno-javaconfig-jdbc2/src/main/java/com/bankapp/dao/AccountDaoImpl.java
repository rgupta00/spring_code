package com.bankapp.dao;

import java.util.*;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
@Repository
//@Primary
public class AccountDaoImpl implements AccountDao{
	
	private Map<Integer, Account> accountsMap=new HashMap<Integer, Account>();
	
	public AccountDaoImpl() {
		accountsMap.put(1, new Account(1, "raj", 1000));
		accountsMap.put(2, new Account(2, "ekta", 1000));
	}
	
	@Override
	public List<Account> getAllAccounts() {
		System.out.println("using collection");
		return new ArrayList<Account>(accountsMap.values());
	}

	@Override
	public void updateAccount(Account account) {
		accountsMap.put(account.getId(), account);
	}

	@Override
	public Account getById(int id) {
		return accountsMap.get(id);
	}
}
