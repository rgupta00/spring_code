package com.demo;
import java.util.*;
//		N
//Shape ----- Point
public class Shape {
	private List<Point> points;

	public List<Point> getPoints() {
		return points;
	}

	public void setPoints(List<Point> points) {
		this.points = points;
	}
	
	public void printShapeDetails() {
		points.forEach(p-> System.out.println(p));
	}

}
