package com.demo;
import java.util.*;
public class Country {
	private String name;
	private Map<String, String> states;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Map<String, String> getStates() {
		return states;
	}
	public void setStates(Map<String, String> states) {
		this.states = states;
	}
	
	public void printCompanyDetails() {
		System.out.println("country name: "+ name);
		System.out.println("state with state capital information");
		states.forEach((state, capital)->{
			System.out.println(state+": "+capital );
		});
	}
	
	

}
