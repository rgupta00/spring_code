package com.demo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MAin {

	public static void main(String[] args) {
		ApplicationContext ctx=new ClassPathXmlApplicationContext("demo.xml");
//		Triangle triangle=(Triangle) ctx.getBean("triagle");
//		triangle.printTriangle();
//		
//		Shape shape=ctx.getBean("shape", Shape.class);
//		
//		shape.printShapeDetails();
		
		Country country=(Country) ctx.getBean("country");
		country.printCompanyDetails();
		
	}
}
