package com.demo1;

public class Passanger {
	private String name;
	private Vehical vehical;
	
	public Passanger() {
	}
	
	//ctr injection
	public Passanger(String name, Vehical vehical) {
		System.out.println("ctr injection is working");
		this.name = name;
		this.vehical = vehical;
	}



	public void travel() {
		System.out.println("name : "+ name);
		vehical.move();
	}

	//i want that this setters must be called spring framework " pull vs push"
	public void setName(String name) {
		this.name = name;
	}

	public void setVehical(Vehical vehical) {
		this.vehical = vehical;
	}
}
