package com.demo1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {
		
		//I was createing the objects with new operator 
		//new XXXX
		//this code will create a container and it will read my xml file demo1.xml
		//it contain configraution of the beans
		ApplicationContext ctx=new ClassPathXmlApplicationContext("demo1.xml");
		
//		Hello hello=ctx.getBean("hello", Hello.class);
//		
//		Hello hello2=ctx.getBean("hello", Hello.class);
//		
//		System.out.println(hello.hashCode());
//		System.out.println(hello2.hashCode());
		
		Passanger passanger=(Passanger) ctx.getBean("passanger");

		passanger.travel();
		
		
		//Passanger ---> Car
//		String name="raj";
//		Vehical vehical=new Bike();
//		
//		//only one problem in this code : i need to manually create the object of car
//		//and inject (assign) inside the passanger object
//		
//		//rather then programmmer doing it it can be done by some external framework
//		// ie spring framework
//		Passanger passanger=new Passanger(name, vehical);
//		passanger.travel();
	}
}
