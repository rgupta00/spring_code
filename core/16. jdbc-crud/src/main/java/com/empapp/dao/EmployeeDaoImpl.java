package com.empapp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.empapp.dao.exceptions.EmployeeNotFoundException;

public class EmployeeDaoImpl implements EmployeeDao {

	private Connection connection;

	public EmployeeDaoImpl() {
		connection = ConnectionFactory.getConnection();
	}

	@Override
	public void addEmployee(Employee e) {
		PreparedStatement pstmt = null;

		try {
			pstmt = connection.prepareStatement("insert into employee(name, email, salary) values (?,?,?)");

			pstmt.setString(1, e.getName());
			pstmt.setString(2, e.getEmail());
			pstmt.setDouble(3, e.getSalary());

			int noOfRowsEffected = pstmt.executeUpdate();
			System.out.println(noOfRowsEffected);

		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void deleteEmployee(int id) {
		PreparedStatement pstmt = null;

		try {
			pstmt = connection.prepareStatement("delete from employee where id=?");
			pstmt.setInt(1, id);
			pstmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updateEmployee(int id, Employee e) {
		PreparedStatement pstmt = null;

		try {
			pstmt = connection.prepareStatement("update employee set email=?, salary =? where id=?");

			pstmt.setString(1, e.getEmail());
			pstmt.setDouble(2, e.getSalary());
			pstmt.setInt(3, id);

			int noOfRowsEffected = pstmt.executeUpdate();
			System.out.println(noOfRowsEffected);

		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	
	//i will create a arraylist
	//i will iterate through all records of table and then create employee object of each record
	//and add that to to employees list and then return that list
			
	@Override
	public List<Employee> getAllEmployee() {
		List<Employee>employees=new ArrayList<Employee>();
		Employee employee=null;
		try {
			PreparedStatement pstmt=connection.prepareStatement("select * from employee");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				employee=
						new Employee(rs.getInt(1),
								rs.getString(2),
								rs.getString(3),
								rs.getDouble(4));
				employees.add(employee);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return employees;
	}

	@Override
	public  Optional<Employee> getEmployeeById(int id) {
		Employee employee=null;
		try {
			PreparedStatement pstmt=connection.prepareStatement("select * from employee where id=?");
			pstmt.setInt(1, id);
			ResultSet rs=pstmt.executeQuery();
			if(rs.next()) {
				employee=
						new Employee(rs.getInt(1),
								rs.getString(2),
								rs.getString(3),
								rs.getDouble(4));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return Optional.ofNullable(employee);
	}

}


/*
 * Employee employee=null;
		try {
			PreparedStatement pstmt=connection.prepareStatement("select * from employee where id=?");
			pstmt.setInt(1, id);
			ResultSet rs=pstmt.executeQuery();
			if(rs.next()) {
				employee=
						new Employee(rs.getInt(1),
								rs.getString(2),
								rs.getString(3),
								rs.getDouble(4));
			}
			else
				throw new EmployeeNotFoundException("employee with id "+ id + " is not found");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
 */


