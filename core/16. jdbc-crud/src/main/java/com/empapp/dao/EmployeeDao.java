package com.empapp.dao;

import java.util.*;

public interface EmployeeDao {
	public void addEmployee(Employee e);

	public void deleteEmployee(int id);

	public void updateEmployee(int id, Employee e);

	public List<Employee> getAllEmployee();

	public Optional<Employee> getEmployeeById(int id);
}
