package com.empapp.dao;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;
public class ConnectionFactory {
	private static Connection connection;
	
	public static Connection getConnection() {

		//i will read the file and then find out drivername, url and username and passwrord from that file
		Properties prop=new Properties();
		
		String driverName=null;
		String url=null;
		String username=null;
		String password=null;
		
		
		try {
			InputStream is=new FileInputStream("db.properties");
			prop.load(is);
			driverName =prop.getProperty("jdbc.driverName");
			url=prop.getProperty("jdbc.url");
			username=prop.getProperty("jdbc.username");
			password=prop.getProperty("jdbc.password");
			
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			Class.forName(driverName);
			System.out.println("driver is loaded....");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		try {
			connection=DriverManager.getConnection
					(url, username, password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return connection;
	}
}
