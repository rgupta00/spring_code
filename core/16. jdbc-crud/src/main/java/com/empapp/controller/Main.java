package com.empapp.controller;

import java.util.*;
import java.util.function.Supplier;

import com.empapp.dao.Employee;
import com.empapp.dao.EmployeeDao;
import com.empapp.dao.EmployeeDaoImpl;
import com.empapp.dao.exceptions.EmployeeNotFoundException;

public class Main {

	public static void main(String[] args) {
		EmployeeDao dao = new EmployeeDaoImpl();
		//Due to inteface i can switch to different tech more easily...
		//jdbc
		//Hibernate
		

		//find by id
		int id=2;
		Optional<Employee> empOpt=dao.getEmployeeById(id);
		
//		Supplier<EmployeeNotFoundException> empSupplier=()-> 
//				 new EmployeeNotFoundException("emp with id " + id +" is not found");
//		
//		
		
		
	//	Employee e=empOpt.orElseThrow(exceptionSupplier);
		
//		Employee emp=empOpt.orElseThrow
//				(()->new EmployeeNotFoundException("emp with id" + id  +" is not found"));
//		
//		System.out.println(emp);
//		
//		emp.setSalary(102);
//		dao.updateEmployee(2, emp);
		
//		Employee employee=dao.getEmployeeById(2)
//				.orElseThrow(()-> new EmployeeNotFoundException("emp not found"));
		
	//	System.out.println(employee);
		
//		employee.setSalary(100);
//		System.out.println(employee);
//		
//		dao.updateEmployee(2, employee);
		
		
		
		
		
		
//		System.out.println("----showing all records----------");
//		List<Employee> employees = dao.getAllEmployee();
//
//		employees.forEach(e -> System.out.println(e));
		
		
		//dao.deleteEmployee(3);
		
		
		

//		System.out.println("---------add new records-------");
//
//		Employee employee = new Employee("indu", "indu@gmail.com", 56);
//
//		dao.addEmployee(employee);
//
//		System.out.println("---------print all rec after adding indu records-------");
//		employees = dao.getAllEmployee();
//		employees.forEach(e -> System.out.println(e));

		

	}
}
