package com.bankapp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.bankapp.dao.AccountDao;
import com.bankapp.dao.AccountDaoImplJdbc;
import com.bankapp.service.AccountService;
import com.bankapp.service.AccountServiceImpl;

@Configuration
@ComponentScan(basePackages = "com.bankapp")
public class AppConfig {

	@Bean(name = "accountservice")
	@Scope("singleton")
	public AccountService getAccountService(AccountDao dao) {
		AccountServiceImpl accountService=new AccountServiceImpl();
		accountService.setAccountDao(dao);
		return accountService;
	}
	@Bean
	public AccountDao getAccountDao() {
		AccountDao dao=new AccountDaoImplJdbc();
		return dao;
	}
}




