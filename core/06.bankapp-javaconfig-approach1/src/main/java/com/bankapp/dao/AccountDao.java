package com.bankapp.dao;
import java.util.*;
public interface AccountDao {
	public List<Account> getAllAccounts();
	public void updateAccount(Account account);
	public Account getById(int id);
}
