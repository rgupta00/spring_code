package com.bankapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Service;

import com.bankapp.dao.Account;
//service layer =dao layer + BL + 
//cross cutting concerns* (sec. tx logging , caching): AOP
import com.bankapp.dao.AccountDao;
import com.bankapp.dao.AccountDaoImpl;
@Service(value = "accountservice")
public class AccountServiceImpl implements AccountService {

	private AccountDao accountDao;
	
	
	
	
//	@Autowired
//	public void setAccountDao(AccountDao accountDao) {
//		this.accountDao = accountDao;
//	}

	@Autowired
	public AccountServiceImpl(AccountDao accountDao) {
		this.accountDao = accountDao;
	}

	@Override
	public List<Account> getAllAccounts() {
		return accountDao.getAllAccounts();
	}

	@Override
	public void fundTransfer(int fromId, int toId, double amount) {
		Account fromAccount=accountDao.getById(fromId);
		Account toAccount=accountDao.getById(toId);
		
		//
		fromAccount.setBalance(fromAccount.getBalance()-amount);
		toAccount.setBalance(toAccount.getBalance()+ amount);
		
		accountDao.updateAccount(fromAccount);
		accountDao.updateAccount(toAccount);
	}

	@Override
	public Account getById(int id) {
		return accountDao.getById(id);
	}

}
