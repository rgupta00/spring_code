package com.customer.controller;

import java.util.Date;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.customer.dao.Customer;
import com.customer.service.CustomerService;

public class Main {

	public static void main(String[] args) {
	
		ApplicationContext ctx=new ClassPathXmlApplicationContext("beans.xml");
		CustomerService customerService=ctx.getBean("cs", CustomerService.class);
		//customerService.getAllCustomers().forEach(c-> System.out.println(c));
		Customer customer=new Customer("ekta", "delhi", "34545454", "e@gmail.com", new Date(), 1);
		customerService.addCustomer(customer);
		
		
		
	}
}

