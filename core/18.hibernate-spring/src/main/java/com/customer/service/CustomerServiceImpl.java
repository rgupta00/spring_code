package com.customer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.customer.dao.Customer;
import com.customer.dao.CustomerDao;
@Service(value = "cs")
@Transactional
public class CustomerServiceImpl implements CustomerService {

	private CustomerDao customerDao;
	@Autowired
	public CustomerServiceImpl(CustomerDao customerDao) {
		this.customerDao = customerDao;
	}

	@Override
	public List<Customer> getAllCustomers() {
		return customerDao.getAllCustomers();
	}

	@Override
	public Customer addCustomer(Customer customer) {
		return customerDao.addCustomer(customer);
	}

	@Override
	public Customer updateCustomer(int id, Customer customer) {
		return customerDao.updateCustomer(id, customer);
	}

	@Override
	public Customer deleteCustomer(int id) {
		return customerDao.deleteCustomer(id);
	}

	@Override
	public Customer getById(int id) {
		return customerDao.getById(id);
	}

	@Override
	public Customer getByName(String name) {
		return customerDao.getByName(name);
	}

}
