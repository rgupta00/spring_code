package com.demo.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.demo.dao.User;
import com.demo.dao.UserDao;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
@Transactional
public class AuthServiceImpl implements AuthService {

	@Autowired
	private UserDao userDao;

	@Override
	public boolean verifyUser(String username, String password) {
		User user = userDao.findByUsernameAndPassword(username, password);
		if (user != null)
			return true;
		else
			return false;
	}

	@Override
	public void addUser(User user) {
		userDao.save(user);
	}

	@Override
	public String generateToken(String username) {
		//i need to write the code to send json jwt token to the user
		return Jwts.builder()
				.setSubject(username)
				.setIssuedAt(new Date())
				.setExpiration(new Date(System.currentTimeMillis()+600000))
				.signWith(SignatureAlgorithm.HS256	, "ustdemo")
				.compact();
	}

}
