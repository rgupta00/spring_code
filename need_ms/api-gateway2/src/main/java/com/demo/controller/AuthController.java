package com.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.dto.LoginRequest;
import com.demo.service.AuthService;
import java.util.*;
@RestController
public class AuthController {

	@Autowired
	private AuthService authService;
	
	@GetMapping("/auth")
	public String hello() {
		return "you are auth to access it...";
	}
	//this method would be unsecre so that user can call it and should able to pass un and pass
	//if un and pw is correrct we are going to give him json web
	//token so that he can access other part of the application
	@PostMapping(path="/login")
	public ResponseEntity<?> loginRequest(@RequestBody LoginRequest loginRequest){
		boolean isValid= authService.verifyUser(loginRequest.getUsername(), loginRequest.getPassword());
		//Map to send the respone to the user if user details are correct i will send jwt token to him otherwise send 403
		Map<String, String> responseMap=new HashMap<String, String>();
		//
		String token =null;// this string will hold the json token
		if(isValid) {
			token =authService.generateToken(loginRequest.getUsername());
			responseMap.put("token", token);
			responseMap.put("message", "login successful");
			return ResponseEntity.ok(responseMap);
		}else{
			responseMap.clear();
			responseMap.put("message", "invalid login");
			return ResponseEntity.status(HttpStatus.FORBIDDEN).body(responseMap);
		}
		
	}
}
