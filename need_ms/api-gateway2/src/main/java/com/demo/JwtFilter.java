package com.demo;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
@Service
public class JwtFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;

		String authHeader = req.getHeader("Authorization");
		// pre flight header
		if (req.getMethod().equals("OPTIONS")) {
			resp.setStatus(resp.SC_OK);
			chain.doFilter(request, response);
		} else {
			if (authHeader == null || !authHeader.startsWith("Bearer ")) {
				resp.sendError(403);
				return ;
			}
			System.out.println(authHeader);
			//Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJyYWoiLCJpYXQiOjE2MjY0MTM3MTcsImV4cCI6MTYyNjQxNDMxN30.xR2N3jVnat1sXDV11hAkIO35emtVmgi6vFpvhVvdzuY
			String token = authHeader.split(" ")[1];
			System.out.println(token);

			try {
				Claims claims = Jwts.parser().
						setSigningKey("ustdemo")
						.parseClaimsJws(token).getBody();
				System.out.println(claims.getSubject());
				request.setAttribute("claims", claims);
				chain.doFilter(request, response);
			} catch (Exception ex) {
				resp.sendError(403);
			}
		}
	}

}
