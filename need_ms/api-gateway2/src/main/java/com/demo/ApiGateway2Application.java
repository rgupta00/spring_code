package com.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

import com.demo.dao.User;
import com.demo.service.AuthService;
@EnableEurekaClient
@EnableZuulProxy
@SpringBootApplication
public class ApiGateway2Application implements CommandLineRunner {

	@Autowired
	private AuthService authService;
	
	public static void main(String[] args) {
		SpringApplication.run(ApiGateway2Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("---------user is added----------");
		authService.addUser(new User("raj", "raj123", "raj@gmail.com"));
	}
	//configure an bean to block other enpoint and configre jwt filter
	@Bean
	public FilterRegistrationBean filterRegistrationBean() {
		FilterRegistrationBean bean=new FilterRegistrationBean();
		bean.setFilter(new JwtFilter());
		bean.addUrlPatterns("/auth/*");
		return bean;
	}
	

}
