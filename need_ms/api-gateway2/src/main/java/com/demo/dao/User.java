package com.demo.dao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@Entity
@Table(name = "user_table")
@NoArgsConstructor
public class User {
	@Id @GeneratedValue(strategy =GenerationType.IDENTITY )
	private int id;
	private String username;
	private String password;
	private String email;
	public User(String username, String password, String email) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
	}
	
	
}
