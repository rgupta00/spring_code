package com.demo.service;

import com.demo.dao.User;

public interface AuthenticationService {
	public boolean verifyUser(String username, String password);
	public void addUser(User user);
	public String generateToken(String username);
}
