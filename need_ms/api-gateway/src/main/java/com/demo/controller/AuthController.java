package com.demo.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.dto.LoginRequest;
import com.demo.service.AuthenticationService;

@RestController
public class AuthController {

	@Autowired
	private AuthenticationService authService;

	@GetMapping("/auth")
	public String homeHander() {
		return "welcome to auth server";
	}

	@PostMapping("/login")
	public ResponseEntity<?> loginHander(@RequestBody LoginRequest loginRequest) {
		System.out.println(loginRequest);
		boolean isValid = authService.verifyUser(loginRequest.getUsername(), loginRequest.getPassword());

		Map<String, String> responseMap = new HashMap<>();
		String token = null;
		if (isValid) {
			token = authService.generateToken(loginRequest.getUsername());
			responseMap.put("token", token);
			responseMap.put("message", "login successfull");
			return ResponseEntity.ok(responseMap);
		} else {
			responseMap.clear();
			responseMap.put("token", null);
			responseMap.put("message", "invalid");
			return ResponseEntity.status(HttpStatus.FORBIDDEN).body(responseMap);
		}

	}
}
