package com.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

import com.demo.dao.User;
import com.demo.service.AuthenticationService;
@EnableZuulProxy
@SpringBootApplication
public class ApiGatewayApplication implements CommandLineRunner {

	@Autowired
	private AuthenticationService authenticationService;
	
	public static void main(String[] args) {
		SpringApplication.run(ApiGatewayApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		authenticationService.addUser(new User("raj", "raj123", "raj@gmail.com"));
	}

	@Bean
	public FilterRegistrationBean filterRegistrationBean(){
		FilterRegistrationBean bean=new FilterRegistrationBean();
		bean.setFilter(new JwtFilter());
		bean.addUrlPatterns("/auth/*");
		return bean;
	}
}
