package com.order.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.order.dto.OrderRequest;
import com.order.entities.Coupon;
@Service
public class CouponService {
	
	@Autowired
	private RestTemplate restTemplate;
	
	@HystrixCommand(fallbackMethod = "getDefaultCoupon")
	public Coupon getCoupon(OrderRequest orderRequest) {
		String couponUrl = "http://COUPON-SERVICE/couponapp/couponbycode/"+orderRequest.getCouponCode();
		Coupon coupon=restTemplate.getForObject(couponUrl, Coupon.class);
		return coupon;
	}

	public Coupon getDefaultCoupon(OrderRequest orderRequest) {
		return new Coupon();
	}
}
