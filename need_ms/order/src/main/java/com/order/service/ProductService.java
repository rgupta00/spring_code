package com.order.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.order.dto.OrderRequest;
import com.order.entities.Product;

@Service
public class ProductService {

	@Autowired
	private RestTemplate restTemplate;
	
	@HystrixCommand(fallbackMethod = "getDefaultProduct")
	public Product getProduct(OrderRequest orderRequest) {
		String productUrl = "http://PRODUCT-SERVICE/productapp/product/"+orderRequest.getPid();
		Product product=restTemplate.getForObject(productUrl,Product.class);
		return product;
	}
	
	private Product getDefaultProduct(OrderRequest orderRequest) {
		return new Product();
	}
}
