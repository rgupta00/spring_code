package com.order.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.order.dto.OrderRequest;
import com.order.entities.Customer;
@Service
public class CustomerService {
	
	@Autowired
	private RestTemplate restTemplate;
	
	@HystrixCommand(fallbackMethod = "getDefaultCustomer")
	public Customer getCustomer(OrderRequest orderRequest) {
		String customerUrl = "http://CUSTOMER-SERVICE/custmerapp/customer/"+orderRequest.getCid();
		Customer customer=restTemplate.getForObject(customerUrl, Customer.class);
		return customer;
	}

	public Customer getDefaultCustomer(OrderRequest orderRequest) {
		return new Customer();
	}
}
