package com.order.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
@Data

@NoArgsConstructor
public class Customer {
	private int id;
	private String name;
	private String email;
	public Customer(String name, String email) {
		super();
		this.name = name;
		this.email = email;
	}
	
	
	
}
