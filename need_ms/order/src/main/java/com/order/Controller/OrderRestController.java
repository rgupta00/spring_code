package com.order.Controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.order.dto.OrderRequest;
import com.order.entities.Coupon;
import com.order.entities.Customer;
import com.order.entities.Order;
import com.order.entities.Product;
import com.order.service.CouponService;
import com.order.service.CustomerService;
import com.order.service.ProductService;

@RestController
public class OrderRestController {

	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private CouponService couponService;
	
	//http://localhost:8080/orderapp/order
	/*
	 * {
    "cid": 1,
    "pid":1,
    "quantity":3,
    "couponCode": "SUP20"
}
	 */
	
	@PostMapping(path="order")
	public ResponseEntity<Order> submitOrder(@RequestBody OrderRequest orderRequest){
		
		//first of all i need to get product infor
		Product product = productService.getProduct(orderRequest);
		
		//i need to get customer info
		Customer customer = customerService.getCustomer(orderRequest);
		
		//i need to get copupon details
		
		Coupon coupon = couponService.getCoupon(orderRequest);
							
		///   50000(100-10)/100
		double discountedPrice= product.getPrice()*(100-coupon.getDiscountPercentage())/100;
		
		double totalPrice= discountedPrice* orderRequest.getQuantity();
		Order order=new Order();
		order.setId(22);
		order.setCustomer(customer);
		order.setProduct(product);
		order.setTotalPrice(totalPrice);
		order.setOrderDate(new Date());
		return ResponseEntity.status(HttpStatus.CREATED).body(order);
		
	}

}
