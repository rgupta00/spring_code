package com.empapp.controller;

import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.empapp.entities.Employee;

@RestController
public class EmpClientRestController {

	@Autowired
	private RestTemplate restTemplate;
//
//	@GetMapping(path = "getanemp/{id}")
//	public Employee getEmployee(@PathVariable(name = "id") int id) {
//		
//		Employee employee = restTemplate
//				.getForObject("http://localhost:8080/empapp/employee/"+id, Employee.class);
//		return employee;
//	}
	

	@GetMapping(path = "getanemp/{id}")
	public Employee getEmployee(@PathVariable(name = "id") String id) {
		String url= "http://localhost:8080/empapp/employee/{id}";
		Map<String, String> params = new HashMap<String, String>();
		params.put("id", id);
		Employee employee = restTemplate
				.getForObject(url, Employee.class, params);
		return employee;
	}
	
	@GetMapping(path = "getempasstring")
	public String getEmployeeAsString() {
		String url= "http://localhost:8080/empapp/employee/5";
	
		String employeeString = restTemplate.getForObject(url, String.class);
		return employeeString;
	}


	@GetMapping(path = "getall")
	public List<?> getAllEmp() {
		String url= "http://localhost:8080/empapp/employee";
	
		List<?> empList=restTemplate.getForObject(url, List.class);
		return empList;
	}
	
	@GetMapping(path = "add")
	public Employee addEmp() {
		String url= "http://localhost:8080/empapp/employee";
	
		Employee request=new Employee("indu", 47);
		
		Employee employee=restTemplate.postForObject(url, request, Employee.class);
		
		return employee;
	}
	
	///------------------- demo xxxforEntity() more powerful method as previous one
	
	@GetMapping(path = "getempv2")
	public Employee getEmpV2() {
		String url= "http://localhost:8080/empapp/employee/5";
		
		ResponseEntity<Employee> entity=restTemplate.getForEntity(url, Employee.class);
		
		Employee employee=entity.getBody();// give u the object
		HttpStatus statusCode = entity.getStatusCode();
		HttpHeaders headers = entity.getHeaders();
		System.out.println(statusCode);
		System.out.println(headers);
		return employee;
		
	}
}

























