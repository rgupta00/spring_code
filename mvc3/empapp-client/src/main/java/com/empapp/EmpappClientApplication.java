package com.empapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class EmpappClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmpappClientApplication.class,args);
	}

	//we need to configure RestTemple
	//it is a way to communicate with other microservice in spring boot projects
	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
}
